#include "StepperULN2003.h"

StepperULN2003::StepperULN2003(int steps, 
                                int in1,
                                int in2,
                                int in3,
                                int in4)
{
    this->in1 = in1;
    this->in2 = in2;
    this->in3 = in3;
    this->in4 = in4;
    this->steps = steps;
    this->stepper = new Stepper(steps, in1, in2, in3, in4);
}

StepperULN2003::~StepperULN2003()
{
    delete this->stepper;
}

void StepperULN2003::move_degrees(long degrees)
{
    int steps = this->get_steps_by_degrees(degrees);
    this->move_steps(steps);
}

void StepperULN2003::move_steps(long steps)
{
    this->stepper->step(steps);
}

void StepperULN2003::set_rotation_speed(long rpms)
{
    this->stepper->setSpeed(rpms);
}

void StepperULN2003::stop_motor()
{
    digitalWrite(this->in1, LOW);
    digitalWrite(this->in2, LOW);
    digitalWrite(this->in3, LOW);
    digitalWrite(this->in4, LOW);
}

int StepperULN2003::get_steps_by_degrees(long degrees)
{
    return map(degrees, 0, 360, 0, this->steps);
}
