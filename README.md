# ino_stepper_uln2003
Library to control a stepper motor with uln2003 driver on Arduino environment

Pre-requirements
--------------
- Installed:
  - arduino-libraries/Stepper@^1.1.3

Physical components
-------------------
- ULN2003 driver

Usage examples
--------------
```cpp

#include "StepperULN2003.h"

#define STEPS_TOTAL   2048
#define DEFAULT_SPEED 4

#define IN1  2
#define IN2  3
#define IN3  4
#define IN4  5

StepperULN2003 *stepper;

void setup()
{
    stepper = new StepperULN2003(STEPS_TOTAL, IN1, IN3, IN2, IN4);
    // init default rotation speed
    stepper->set_rotation_speed(DEFAULT_SPEED);
}

void loop()
{
    // move stepper in regular direction
    stepper->move_steps(1);

    // move stepper in the opposite direction
    stepper->move_steps(-1);

    // change rotation speed
    stepper->set_rotation_speed(DEFAULT_SPEED);

    // rotate stepper 30 degrees
    stepper->move_degrees(30);

    // safe stop stepper rotation
    stepper->stop_motor(DEFAULT_SPEED);
}

```
