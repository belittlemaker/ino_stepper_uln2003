#ifndef StepperULN2003_h
    #define StepperULN2003_h
    
    #include <Arduino.h>
    #include <Stepper.h>
    
    class StepperULN2003
    {      
        public:        
            StepperULN2003(int steps, 
                            int in1,
                            int in2,
                            int in3,
                            int in4);
            ~StepperULN2003();
            void move_degrees(long degrees_);
            void move_steps(long steps);
            void set_rotation_speed(long rpms);
            void stop_motor();
        private:
            int in1;
            int in2;
            int in3;
            int in4;
            int steps;
            Stepper* stepper;
            int get_steps_by_degrees(long degrees_);
    };

#endif // StepperULN2003_h
